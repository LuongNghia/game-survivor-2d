using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolController : MonoBehaviour
{
    private static ObjectPoolController instance;

    [SerializeField]
    protected GameObject bullet;
    protected List<GameObject> poolObjects = new List<GameObject>();
    protected int amoutToPool = 40;
    public static ObjectPoolController Instance { get => instance; }
    [SerializeField]
    protected Transform bulletHolder;

        private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < amoutToPool; i++)
        {
            GameObject obj = Instantiate(bullet);
            obj.transform.parent = bulletHolder;
            obj.SetActive(false);
            poolObjects.Add(obj);
        }    
    }

    public virtual GameObject GetPoolObject()
    {
        for (int i = 0; i < poolObjects.Count; i++)
        {
            if(!poolObjects[i].activeInHierarchy)
            {
                return poolObjects[i];
            }
        }
        return null;
    }

    
}
