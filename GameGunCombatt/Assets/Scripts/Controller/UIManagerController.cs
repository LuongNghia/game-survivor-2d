using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManagerController : MonoBehaviour
{
    private static UIManagerController instance;

    [SerializeField]
    private List<Image> listImage;
    [SerializeField]
    protected Text txtCoin;
    [SerializeField]
    protected Text txtBomb;

    public List<Image> ListImage { get => listImage; set => listImage = value; }
    public static UIManagerController Instance { get => instance; }

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    private void Start()
    {
        txtCoin.text = "0 x";
        txtBomb.text = "0 x";
    }

    private void Update()
    {
        EditUIHealth();
        EditUIItem();
    }

    protected virtual void EditUIItem()
    {
        if (txtCoin != null)
        {
            PlayerPrefs.SetFloat("CoinTxt", ItemController.Instance.CountCoin);
            txtCoin.text = ItemController.Instance.CountCoin + " x";
        }
        if (txtBomb != null)
            txtBomb.text = ItemController.Instance.CountBomb + " x";
    }
    
    protected virtual void EditUIHealth()
    {
        for (int i = 0; i < listImage.Count; i++)
        {
            if (PlayerHeathController.Instace.CurrentHealth == 5)
            {
                listImage[5].gameObject.SetActive(false);
            }
            if (PlayerHeathController.Instace.CurrentHealth == 4)
            {
                listImage[4].gameObject.SetActive(false);
            }
            if (PlayerHeathController.Instace.CurrentHealth == 3)
            {
                listImage[3].gameObject.SetActive(false);
            }
            if (PlayerHeathController.Instace.CurrentHealth == 2)
            {
                listImage[2].gameObject.SetActive(false);
            }
            if (PlayerHeathController.Instace.CurrentHealth == 1)
            {
                listImage[1].gameObject.SetActive(false);
            }
            if (PlayerHeathController.Instace.CurrentHealth == 0)
            {
                listImage[0].gameObject.SetActive(false);
            }
        }
/*        GameObject player = FindObjectOfType<Player>().gameObject;
        if(player.activeInHierarchy == false)
        {
            listImage[1].gameObject.SetActive(false);
            listImage[3].gameObject.SetActive(false);
            listImage[5].gameObject.SetActive(false);
            listImage[2].gameObject.SetActive(false);
            listImage[4].gameObject.SetActive(false);
        }*/
    }
}
