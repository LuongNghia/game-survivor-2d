using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHomeController : MonoBehaviour
{
    private static UIHomeController instance;

    public static UIHomeController Instance { get => instance; }
    public GameObject DialogShop { get => dialogShop; set => dialogShop = value; }
    public GameObject DialogSetting { get => dialogSetting; set => dialogSetting = value; }
    public GameObject DialogUpgrade { get => dialogUpgrade; set => dialogUpgrade = value; }
    public GameObject DialogAssistants { get => dialogAssistants; set => dialogAssistants = value; }
    public GameObject DialogWeapons { get => dialogWeapons; set => dialogWeapons = value; }
    public GameObject DialogNotification { get => dialogNotification; set => dialogNotification = value; }
/*    public int CurTotalCoin { get => curTotalCoin; set => curTotalCoin = value; }*/
    public Text TxtDialog { get => txtDialog; set => txtDialog = value; }
    public Coin Coin { get => coin; set => coin = value; }
    public GameObject DialogSupport { get => dialogSupport; set => dialogSupport = value; }

    [SerializeField]
    private GameObject dialogShop;
    [SerializeField]
    private GameObject dialogSetting;
    [SerializeField]
    private GameObject dialogUpgrade;
    [SerializeField]
    private GameObject dialogAssistants;
    [SerializeField]
    private GameObject dialogWeapons;
    //dialog
    [SerializeField]
    private GameObject dialogNotification;
    [SerializeField]
    private Text txtDialog;
    [SerializeField]
    private GameObject dialogSupport;

    [SerializeField]
    private Text txtTotalCoin;
/*    private int curTotalCoin;*/
    [SerializeField]
    private Coin coin;

/*    private void Start()
    {
        curTotalCoin = coin.totalCoin;
    }*/

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    private void Update()
    {
        txtTotalCoin.text = coin.totalCoin + "";
    }
}
