using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAwarenessController : MonoBehaviour
{
    public bool AwareOfPlayer { get; private set; }
    public Vector2 DirectionToPlayer { get; private set; }
    public static PlayerAwarenessController Instance { get => instance; }

    private static PlayerAwarenessController instance;

    [SerializeField]
    private float _playerAwarenessDistance;

    private Transform _player;

    private void Awake()
    {
        if(instance == null)
            instance = this;
        _player = FindObjectOfType<Player>().transform;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 enemyToPlayer = _player.position - transform.position;
        DirectionToPlayer = enemyToPlayer.normalized;
        if(enemyToPlayer.magnitude <= _playerAwarenessDistance)
        {
            AwareOfPlayer = true;
        } else
        {
            AwareOfPlayer = false;
        }
    }
}
