using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemyController : MonoBehaviour
{
    //length object
    [SerializeField]
    protected int enemyObjectFly;
    [SerializeField]
    protected int enemyObjectWalk;
    [SerializeField]
    protected int enemyObjectRoll;
    [SerializeField]
    protected int enemyObjectBomb;

    //object
    [SerializeField]
    protected GameObject enemyFly;
    [SerializeField]
    protected GameObject enemyWalk;
    [SerializeField]
    protected GameObject enemyRoll;
    [SerializeField]
    protected GameObject enemyBomb;
    private int countLength = 5;

    //time delay
    private float timeRate = 30f;
    private float curTimeRate;

    Vector3 spawnLocation;

    private void Start()
    {
        curTimeRate = timeRate;
        StartCoroutine(StartCreateEnemy());
    }
    IEnumerator StartCreateEnemy()
    {
        CreateObjectEnemyFly();
        CreateObjectEnemyWalk();
        CreateObjectEnemyRoll();
        CreateObjectEnemyBomb();
        yield return null;
    }

    private void Update()
    {
        if(curTimeRate > 0)
        {
            curTimeRate -= Time.deltaTime;
        } else
        {
            EnemyAL.Instance.SpawnDelay = 5f;
            curTimeRate = 0f;
        }
    }

    /*    private void CreateObjectEnemyFly()
        {
            Vector3 playPos = FindObjectOfType<Player>().transform.position;
            Vector2 findTarget = (Vector2)playPos + (Random.Range(10f, 15f) * new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized);
            for (int i = 0;i<enemyObjectFly;i++)
            {
                GameObject obj = Instantiate(enemyFly); 
                obj.transform.position = new Vector3(findTarget.x, findTarget.y, 0f);
            }
        }

        private void CreateObjectEnemyWalk()
        {
            Vector3 playerPos = FindObjectOfType<Player>().transform.position;
            Vector2 findTarget = (Vector2)playerPos + (Random.Range(10f, 15f) * new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized);
            for (int i = 0; i < enemyObjectWalk; i++)
            {
                GameObject obj = Instantiate(enemyWalk);
                obj.transform.position = new Vector3(findTarget.x, findTarget.y + countLength, 0f);
                countLength += 5;
            }
        }

        private void CreateObjectEnemyRoll()
        {
            Vector3 playerPos = FindObjectOfType<Player>().transform.position;
            Vector2 findTarget = (Vector2)playerPos + (Random.Range(-10f, -15f) * new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized);
            for (int i = 0; i < enemyObjectRoll; i++)
            {
                GameObject obj = Instantiate(enemyRoll);
                obj.transform.position = new Vector3(findTarget.x - countLength, findTarget.y, 0f);
                countLength += 5;
            }
        }

        private void CreateObjectEnemyBomb()
        {
            Vector3 playerPos = FindObjectOfType<Player>().transform.position;
            Vector2 findTarget = (Vector2)playerPos + (Random.Range(-10f, -15f) * new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized);
            for (int i = 0; i < enemyObjectBomb; i++)
            {
                GameObject obj = Instantiate(enemyBomb);
                obj.transform.position = new Vector3(findTarget.x + countLength, findTarget.y, 0f);
                countLength += 5;
            }
        }*/

    private void CreateObjectEnemyFly()
    {
        spawnLocation = GetSpawnLocation(transform.position, 24f);
        for (int i = 0; i < enemyObjectFly; i++)
        {
            GameObject obj = Instantiate(enemyFly);
            obj.transform.position = spawnLocation;
        }
    }

    private void CreateObjectEnemyWalk()
    {
        spawnLocation = GetSpawnLocation(transform.position, 24f);
        for (int i = 0; i < enemyObjectWalk; i++)
        {
            GameObject obj = Instantiate(enemyWalk);
            obj.transform.position = new Vector3(spawnLocation.x, spawnLocation.y + countLength, spawnLocation.z);
            countLength += 4;
        }
    }

    private void CreateObjectEnemyRoll()
    {
        spawnLocation = GetSpawnLocation(transform.position, 24f);
        for (int i = 0; i < enemyObjectRoll; i++)
        {
            GameObject obj = Instantiate(enemyRoll);
            obj.transform.position = new Vector3(spawnLocation.x - countLength, spawnLocation.y, spawnLocation.z);
            countLength += 4;

        }
    }

    private void CreateObjectEnemyBomb()
    {
        spawnLocation = GetSpawnLocation(transform.position, 24f);
        for (int i = 0; i < enemyObjectBomb; i++)
        {
            GameObject obj = Instantiate(enemyBomb);
            obj.transform.position = new Vector3(spawnLocation.x + countLength, spawnLocation.y, spawnLocation.z);
            countLength += 4;
        }
    }

    private Vector3 GetSpawnLocation(Vector3 center, float radius)
    {
        float angle = Random.value * 360;
        Vector3 pos;
        pos.x = center.x + radius * Mathf.Sin(angle * Mathf.Deg2Rad);
        pos.y = center.y + radius * Mathf.Cos(angle * Mathf.Deg2Rad);
        pos.z = center.z;
        return pos;
    }
}
