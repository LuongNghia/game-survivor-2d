using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{

    [SerializeField]
    protected float speed;
    [SerializeField]
    protected float rotateSpeed;

    private Rigidbody2D rg;
    private Vector2 _targetDirection;

    private void Awake()
    {
        rg = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        UpdateTargetDirection();
        RotateTarget();
        SetVelocity();
    }

    protected virtual void UpdateTargetDirection()
    {
        if(PlayerAwarenessController.Instance.AwareOfPlayer)
        {
            _targetDirection = PlayerAwarenessController.Instance.DirectionToPlayer;
        } else
        {
            _targetDirection = Vector2.zero;
        }
    }

    protected virtual void RotateTarget()
    {
        if (_targetDirection == Vector2.zero) return;

        Quaternion targetRotation = Quaternion.LookRotation(transform.forward, _targetDirection);
        Quaternion rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotateSpeed * Time.fixedDeltaTime);
        rg.SetRotation(rotation);
    }

    protected virtual void SetVelocity()
    {
        if(_targetDirection == Vector2.zero)
        {
            rg.velocity = Vector2.zero;
        } else
        {
            rg.velocity = transform.up * speed;
        }
    }
}
